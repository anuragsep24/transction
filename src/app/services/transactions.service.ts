import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable,BehaviorSubject, throwError } from "rxjs";
import { Transaction } from '../modals/transaction.modal';

@Injectable()
export class TransactionsService {
    constructor(private httpClient: HttpClient) {
      }
      baseUrl:string = `${window.location.origin}/assets/mock/`

      /**
       * Get the list of Transactions from the server
       */
      getTransactionList():Observable<any>{
          return this.httpClient.get(this.baseUrl+'transactions.json');
      }

      private transactionAdd = new BehaviorSubject<Transaction>(null);
      transactionAddObsrv = this.transactionAdd.asObservable();
      setTransactionAdd(transaction:Transaction){
          this.transactionAdd.next(transaction);
      }
}