import { Pipe, PipeTransform, Injectable } from '@angular/core';

@Pipe({
    name: 'searchPipe',
    pure: false
})
@Injectable()
export class SearchPipe implements PipeTransform {

    /**
     * @items = object array
     * @term = term's search
     */
    transform(items: any, term: any): any {
        if (term === undefined) return items;

        return items.filter(function (item) {
            for (let property in item) {
                if (item[property] === null) {
                    continue;
                } else if (property === 'merchant' || property === 'transactionType' || property === 'amount') {
                    if (item[property].toString().toLowerCase().includes(term.toLowerCase())) {
                        return true;
                    }
                }
            }
            return false;
        });
    }
}