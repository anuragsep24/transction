import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'orderByPipe',
    pure: false
})
export class OrderByPipe implements PipeTransform {
    /**
    * @items = object array
    * @filed = field to be sort
    * @sortType =  ascending or descending
    */
    transform(items: Array<any>, field: string, sortType: boolean): Array<any> {
        items.sort((a: any, b: any) => {
            let ae = a[field];
            let be = b[field];
            if (ae == undefined && be == undefined) return 0;
            if (ae == undefined && be != undefined) return sortType ? 1 : -1;
            if (ae != undefined && be == undefined) return sortType ? -1 : 1;
            if (ae == be) return 0;
            if(field.toString().toLowerCase().includes('date') || field.toString().toLowerCase().includes('amount')){
                let av = Number(ae);
                let bv = Number(be);
                return sortType ? (av > bv ? -1 : 1) : (bv > av ? -1 : 1);
            }
            return sortType ? (ae.toString().toLowerCase() > be.toString().toLowerCase() ? -1 : 1) : (be.toString().toLowerCase() > ae.toString().toLowerCase() ? -1 : 1);
        });
        return items;
    }
}