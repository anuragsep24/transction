
import { NgModule } from '@angular/core';
import { SearchPipe } from '../pipes/search.pipe';
import { OrderByPipe } from '../pipes/orderBy.pipe'; 

@NgModule({
  declarations: [
    SearchPipe,
    OrderByPipe
  ],
  exports: [ 
    SearchPipe,
    OrderByPipe
  ],
  providers: []
})
export class PipesModule { }
