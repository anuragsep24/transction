import { Component, OnInit } from '@angular/core';

import { TransactionsService } from '../../services/transactions.service';
import { Transaction } from '../../modals/transaction.modal';
import { DomSanitizer } from '@angular/platform-browser';
import { OrderByPipe } from '../../pipes/orderBy.pipe';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css'],
  providers: [OrderByPipe]
})
export class TransactionListComponent implements OnInit {
  title = 'transactions';


  transactionsList: Array<Transaction> = [];
  sortType: boolean;
  sortField: string;

  constructor(private transactionsService: TransactionsService, private domSanitizer: DomSanitizer, private orderByPipe: OrderByPipe) {
    this.sortType = true;
    this.sortField = 'transactionDate';
  }
  /**
   * toggle the sorting behaviour 
   * @param field a field that need to be sorted out
   */
  toggleSort(field: string) {
    this.sortType = !this.sortType;
    this.sortField = field;
    this.transactionsList = this.orderByPipe.transform(this.transactionsList, this.sortField, this.sortType);
  }


  ngOnInit() {
    //get transaction List on init
    this.transactionsService.getTransactionList().subscribe(data => {
      this.transactionsList = data['data'];
      this.transactionsList = this.orderByPipe.transform(this.transactionsList, this.sortField, this.sortType);
    }, error => { console.log(error) });

    //subscribe for any change in new transaction
    this.transactionsService.transactionAddObsrv.subscribe(data => {
      let transactionObj: Transaction;
      transactionObj = data;
      if (transactionObj && transactionObj != null) {
        transactionObj.categoryCode = this.transactionsList[0].categoryCode;
        transactionObj.merchantLogo = this.transactionsList[0].merchantLogo;
        this.transactionsList.push(transactionObj);
        this.transactionsList = this.orderByPipe.transform(this.transactionsList, this.sortField, this.sortType);
      }
    }, error => { console.log(error) });
  }

}
