import { TestBed, async } from '@angular/core/testing';
import { TransactionListComponent } from './transaction-list.component';
import { SearchPipe } from '../../pipes/search.pipe';
import { OrderByPipe } from '../../pipes/orderBy.pipe'; 
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TransactionsService } from '../../services/transactions.service';
import { PipesModule } from '../../pipes/pipes.module';

describe('TransactionListComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TransactionListComponent
      ],
      imports:[
        FormsModule,ReactiveFormsModule,HttpClientModule,PipesModule
      ],
      providers:[TransactionsService]
    }).compileComponents();
  }));

 

  it(`should have as title 'transactions'`, () => {
    const fixture = TestBed.createComponent(TransactionListComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('transactions');
  });

  
});
