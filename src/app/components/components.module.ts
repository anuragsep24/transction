import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';

import { AppComponent } from './app/app.component';
import { PipesModule } from '../pipes/pipes.module';

import { TransactionsService } from '../services/transactions.service';
import { TransactionComponent } from './transaction/transaction.component';
import { TransactionListComponent } from './transaction-list/transaction-list.component';

@NgModule({
  declarations: [
    AppComponent,
    TransactionComponent,
    TransactionListComponent
  ],
  imports: [
    FormsModule,ReactiveFormsModule,CommonModule,PipesModule
  ],
  providers: [TransactionsService]
})
export class ComponentsModule { }
