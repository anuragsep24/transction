import { TestBed, async } from '@angular/core/testing';
import { TransactionComponent } from './transaction.component';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TransactionsService } from '../../services/transactions.service';

describe('TransactionComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TransactionComponent
      ],
      imports:[
        FormsModule,ReactiveFormsModule,HttpClientModule
      ],
      providers:[TransactionsService]
    }).compileComponents();
  }));
  it(`should have as fromAccountBalance ' 5825.76'`, () => {
    const fixture = TestBed.createComponent(TransactionComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.fromAccountBalance).toEqual( 5825.76);
  });

  
});
