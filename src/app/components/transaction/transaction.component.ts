import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TransactionsService } from '../../services/transactions.service';
import { Transaction } from '../../modals/transaction.modal';
import { OrderByPipe } from '../../pipes/orderBy.pipe';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css'],
  providers: [OrderByPipe]
})
export class TransactionComponent implements OnInit {

  transactionForm: FormGroup;
  submitted: boolean;
  fromAccount: String;
  fromAccountBalance: number;
  toAccount: string;
  transferAmount: number;

  constructor(private transactionsService: TransactionsService, private formBuilder: FormBuilder) {
    this.fromAccount = 'Free Checking (4692)';
    this.fromAccountBalance = 5825.76;
    this.toAccount = '';
    //Construct the form 
    this.transactionForm = this.formBuilder.group({
      receiver: ['', Validators.required],
      amount: [null, [Validators.required,Validators.min(1)]]
    });
    this.submitted = false;
  }

 
  /**
   * Form submission action method
   */
  onSubmit() {
    // stop here if form is invalid
    this.submitted = true;
    if (this.transactionForm.invalid) {
      return;
    } else {
      let fValue: any = this.transactionForm.value;
      if (fValue.amount < this.fromAccountBalance + 500) {
        //Construct new Transaction object
        let d: any = (new Date()).getTime();
        let transactionObj: Transaction = new Transaction(fValue.amount,'',fValue.receiver,'',d.toString(),'Online Transfer');
        this.transactionsService.setTransactionAdd(transactionObj);
        this.fromAccountBalance = this.fromAccountBalance - fValue.amount;
        this.transactionForm.reset();
        this.submitted = false;
      }else(
        alert('Your credit value is exceeded.')
      )

    }
  }

  ngOnInit() {   
  }

}
