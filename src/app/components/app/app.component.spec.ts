import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TransactionsService } from '../../services/transactions.service';
import { TransactionComponent } from '../transaction/transaction.component';
import { TransactionListComponent } from '../transaction-list/transaction-list.component';
import { PipesModule } from '../../pipes/pipes.module';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        TransactionComponent,
        TransactionListComponent
      ],
      imports:[
        FormsModule,ReactiveFormsModule,HttpClientModule,PipesModule
      ],
      providers:[TransactionsService]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'transactions'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('transactions');
  });

  
});
