export class Transaction{
    constructor(
        public amount:number,
        public categoryCode:string,
        public merchant:string,
        public merchantLogo:string,
        public transactionDate:string,
        public transactionType:string){}
}